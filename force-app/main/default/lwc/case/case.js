import { LightningElement } from 'lwc';
export default class Case extends LightningElement {
    openLink = false;
    websiteLabel = true;


    gotowebsite(event) {
        this.openLink = true;   
        this.websiteLabel = !this.websiteLabel
        if(this.websiteLabel){
            this.openLink = false; 
        }
        
    }
}

